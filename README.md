# Projet ALGAV

Projet de l'UE Algorithmique Avancée du Master d'informatique de Sorbonne Université (2020/2021).

## Description

Le projet consiste à comparer les performances de l'algorithme Welzl pour le [calcul d'un cercle minimum](https://fr.wikipedia.org/wiki/Probl%C3%A8me_du_cercle_minimum) 
avec celles d'un algorithme naïf. 

On trouvera une implémentation d'un algorithme naïf dans le fichier `naive.rs`. Celui-ci est adapté d'une solution donnée 
en Java par Binh-Minh Bui-Xuan au TME5 de l'UE ALGAV.

L'implémentation de l'algorithme Welzl se trouve dans `welzl.rs`. 

## Utilisation

Pour lancer le programme, on exécutera le fichier `main` à la racine. 

Pour spécifier quel algorithme utiliser, on passera les commandes `-n` (naïf) ou `-w` (Welzl); elles peuvent être utilisées 
simultanément. 

Le programme dispose de deux modes : 

* Dessin d'une instance : avec l'argument `-d n` où `n` correspond au numéro de l'instance à dessiner, enregistre le résultat
des deux algorithmes dans des fichiers `naive.svg` et `welzl.svg` dans le dossier `target`.
* Exécution chronométrée : avec l'argument `-r n` où `n` correspond au nombre d'instances (contenant chacune 256 points)
à charger en même temps. 
  
## A propos des instances de test

Pour les tests, j'ai utilisé des instances fournies par Binh-Minh Bui-Xuan. 

On peut ajouter des instances de test dans un dossier `samples` sous le nom `test-n.points` (où `n` est un nombre entre 
2 et 1663). Celles-ci doivent contenir deux entiers par ligne, chaque ligne représentant un point. 
