use crate::utils::*;

/// Naive algorithm to compute the smallest enclosing disc of a list of points.
/// Adapted from a solution given by Binh-Minh Bui-Xuan.
pub fn compute(list: &Vec<(isize, isize)>) -> Option<Circle> {
    match list.len() {
        0 => None,
        1 => {
            let (x, y) = list.get(0).unwrap();
            Some(Circle::new((*x, *y), 0.))
        }
        2 => {
            let pt1 = list.get(0).unwrap();
            let pt2 = list.get(1).unwrap();
            let x = (pt1.0 + pt2.0) / 2;
            let y = (pt1.1 + pt2.1) / 2;
            let r = (distance_squared(*pt1, *pt2) as f64).sqrt() / 2.;
            Some(Circle::new((x, y), r))
        }
        _ => {
            let mut c_x;
            let mut c_y;
            let mut c_radius_squared;
            for p in list {
                for q in list {
                    c_x = 0.5 * ((*p).0 + (*q).0) as f64;
                    c_y = 0.5 * ((*p).1 + (*q).1) as f64;
                    c_radius_squared = 0.25
                        * (((*p).0 - (*q).0) * ((*p).0 - (*q).0)
                            + ((*p).1 - (*q).1) * ((*p).1 - (*q).1))
                            as f64;
                    let c = Circle::new((c_x as isize, c_y as isize), c_radius_squared.sqrt());
                    if c.contains_all(&list) {
                        return Some(c);
                    }
                }
            }
            let mut res_x = 0;
            let mut res_y = 0;
            let mut res_radius_squared = f64::MAX;
            let size = list.len();
            for i in 0..size {
                for j in (i + 1)..size {
                    for k in (j + 1)..size {
                        // These unwrap are safe because i, j and k are between 0 and list.len()
                        let mut p = list.get(i).unwrap();
                        let mut q = list.get(j).unwrap();
                        let mut r = list.get(k).unwrap();
                        // If the points are collinear, we continue
                        if ((*q).0 - (*p).0) * ((*r).1 - (*p).1)
                            - ((*q).1 - (*p).1) * ((*r).0 - (*p).0)
                            == 0
                        {
                            continue;
                        }
                        // If p and q (or p and r) are on the same line, we swap them
                        if (*p).1 == (*q).1 {
                            p = list.get(k).unwrap();
                            r = list.get(i).unwrap();
                        } else if (*p).1 == (*r).1 {
                            p = list.get(j).unwrap();
                            q = list.get(i).unwrap();
                        }
                        let m_x = 0.5 * ((*p).0 + (*q).0) as f64;
                        let m_y = 0.5 * ((*p).1 + (*q).1) as f64;
                        let n_x = 0.5 * ((*p).0 + (*r).0) as f64;
                        let n_y = 0.5 * ((*p).1 + (*r).1) as f64;
                        let alpha1 = ((*q).0 - (*p).0) as f64 / ((*p).1 - (*q).1) as f64;
                        let beta1 = m_y - alpha1 * m_x;
                        let alpha2 = ((*r).0 - (*p).0) as f64 / ((*p).1 - (*r).1) as f64;
                        let beta2 = n_y - alpha2 * n_x;
                        c_x = (beta2 - beta1) / (alpha1 - alpha2);
                        c_y = alpha1 * c_x + beta1;
                        c_radius_squared = ((*p).0 as f64 - c_x) * ((*p).0 as f64 - c_x)
                            + ((*p).1 as f64 - c_y) * ((*p).1 as f64 - c_y);
                        if c_radius_squared >= res_radius_squared {
                            continue;
                        }
                        let c = Circle::new((c_x as isize, c_y as isize), c_radius_squared.sqrt());
                        if c.contains_all(&list) {
                            res_x = c_x as isize;
                            res_y = c_y as isize;
                            res_radius_squared = c_radius_squared;
                        }
                    }
                }
            }
            Some(Circle::new((res_x, res_y), res_radius_squared.sqrt()))
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::utils::Circle;

    #[test]
    fn test_compute() {
        let pts0 = vec![];
        let pts1 = vec![(1, 1)];
        let pts2 = vec![(2, 2), (-2, 2)];
        let pts3 = vec![(-1, 0), (0, -1), (1, 0)];

        let c1 = Circle::new((1, 1), 0.);
        let c2 = Circle::new((0, 2), 2.);
        let c3 = Circle::new((0, 0), 1.);

        assert_eq!(compute(&pts0), None);
        assert_eq!(compute(&pts1), Some(c1));
        assert_eq!(compute(&pts2), Some(c2));
        assert_eq!(compute(&pts3), Some(c3));
    }
}
