use crate::utils::parse_sample;
use draw::*;
use std::time::Instant;
use structopt::StructOpt;

mod naive;
mod utils;
mod welzl;

/// Configuration options
#[derive(StructOpt, Debug)]
#[structopt(name = "config")]
struct Opt {
    /// Draws the resulting circles (naive and welzl algorithms) in svg files for instance n
    #[structopt(short = "d", long, default_value = "0")]
    draw: usize,

    /// Runs the algorithms, n instances of 256 points at a time
    #[structopt(short = "r", long, default_value = "0")]
    run: usize,

    /// Use the naive algorithm
    #[structopt(short = "n", long)]
    naive: bool,

    /// Use the Welzl algorithm
    #[structopt(short = "w", long)]
    welzl: bool,
}

/// A method to visually check the algorithms.
/// Uses either naive or Welzl algorithm (according to the is_naive parameter) on a list of points.
/// Saves the resulting draw in a SVG file in the "target" folder.
fn draw(points: &Vec<(isize, isize)>, is_naive: bool) {
    // Make the canvas
    let mut canvas = Canvas::new(1000, 1000);
    // Make a white background
    let back = Drawing::new()
        .with_shape(Shape::Rectangle {
            width: 1000,
            height: 1000,
        })
        .with_xy(0., 0.)
        .with_style(Style::filled(RGB::new(255, 255, 255)));
    canvas.display_list.add(back);

    // Make the circle
    let c = if is_naive {
        naive::compute(&points.iter().cloned().collect()).unwrap()
    } else {
        welzl::minidisk(&mut points.iter().cloned().collect())
    };
    let color = if is_naive {
        RGB::new(255, 0, 0)
    } else {
        RGB::new(0, 255, 0)
    };
    // Draw it
    let c_draw = Drawing::new()
        .with_shape(Shape::Circle {
            radius: c.get_radius() as u32,
        })
        .with_xy(c.get_x() as f32, c.get_y() as f32)
        .with_style(Style::stroked(5, color));
    // Add it to the canvas
    canvas.display_list.add(c_draw);

    // Add the points to the canvas
    for p in points {
        let p_draw = Drawing::new()
            .with_shape(Shape::Circle { radius: 1 })
            .with_xy(p.0 as f32, p.1 as f32)
            .with_style(Style::stroked(1, RGB::new(0, 0, 255)));
        canvas.display_list.add(p_draw);
    }

    // Render the full drawing in a SVG file
    if is_naive {
        render::save(&canvas, "target/naive.svg", SvgRenderer::new()).expect("Error saving naive");
        println!("Saved naive circle");
    } else {
        render::save(&canvas, "target/welzl.svg", SvgRenderer::new()).expect("Error saving welzl");
        println!("Saved welzl circle");
    }
}

/// A method to run timed computations of naive and/or Welzl algorithm for a list of points.
fn run(mut points: Vec<(isize, isize)>, naive: bool, welzl: bool) -> (u128, u128) {
    let mut dt1 = 0;
    let mut dt2 = 0;

    if naive {
        let t1 = Instant::now();
        naive::compute(&points);
        dt1 = t1.elapsed().as_millis();
        println!("Naive : {} ms", dt1);
    }

    if welzl {
        let t2 = Instant::now();
        welzl::minidisk(&mut points);
        dt2 = t2.elapsed().as_millis();
        println!("Welzl : {} ms", dt2);
    }

    println!();
    (dt1, dt2)
}

fn main() {
    // Parsing arguments
    let opt = Opt::from_args();
    if opt.draw != 0 {
        // Draw mode
        let points = utils::parse_sample(opt.draw);
        if opt.naive {
            draw(&points, true);
        }
        if opt.welzl {
            draw(&points, false);
        }
    } else if opt.run != 0 {
        // Run mode
        if opt.run < 1 || opt.run > 1663 {
            println!("Arg error : must be between 1 and 1663.")
        } else {
            let mut sum_naive = 0;
            let mut sum_welzl = 0;
            let mut n = 0;
            let mut inf = 2; /*inferior bound of the instance group*/
            let mut sup = inf + opt.run; /*superior bound of the instance group*/
            let cap = (100 / (opt.run * opt.run)) as u128;

            // We have 1663 test instances numbered from 2 to 1664, so sup should not be over 1664
            // Also, when using naive algorithm, we don't want too much tests as it takes a lot of time
            // so we use cap as the maximum number of instance goup we test
            while (!opt.naive || n < cap) && sup < 1665 {
                let mut points = Vec::new();
                for i in inf..sup {
                    points.append(&mut parse_sample(i));
                }
                let (p, q) = run(points, opt.naive, opt.welzl);
                sum_naive += p;
                sum_welzl += q;
                n += 1;
                inf = sup;
                sup += opt.run;
            }
            println!("Average time for Naive : {} ms", sum_naive / n);
            println!("Average time for Welzl : {} ms", sum_welzl / n);
        }
    } else {
        println!("Required argument : -d n to draw OR -r n to run tests.")
    }
}
