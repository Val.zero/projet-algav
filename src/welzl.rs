use crate::utils::CircleSituation::Outside;
use crate::utils::*;
use rand::{thread_rng, Rng};

/// Implementation of the first procedure of Welzl algorithm to compute the minimum enclosing disc of a list of points.
pub fn minidisk(list: &mut Vec<(isize, isize)>) -> Circle {
    b_minidisk(list, &mut Vec::new()).unwrap()
}

/*/// Implementation of the first procedure of Welzl algorithm to compute the minimum enclosing disc of a list of points.
fn minidisk_orig(list: &mut Vec<(isize, isize)>) -> Option<Circle> {
    let res;
    let size = list.len();
    if size == 0 {
        // The min circle for the empty set is empty itself
        res = None;
    } else {
        // let mut rng = thread_rng();
        // let r = rng.gen_range(0, size);
        // let p = list.remove(r);
        let p = list.pop().unwrap();
        match minidisk_orig(list) {
            None => {
                // An empty circle means we previously had 1 element in our set
                res = Some(Circle::new((p.0, p.1), 0.));
            }
            Some(d) => {
                if d.contains(p) == Outside {
                    res = b_minidisk(list, &mut vec![p]);
                } else {
                    res = Some(d);
                }
            }
        }
    }
    res
}*/

/// Implementation of the second procedure of Welzl algorithm to compute the minimum enclosing disc of a list of points.
fn b_minidisk(list: &mut Vec<(isize, isize)>, border: &mut Vec<(isize, isize)>) -> Option<Circle> {
    let res;
    let size = list.len();
    if size == 0 || border.len() == 3 {
        res = compute(border);
        // Not sure about this, but don't see where else (and how) to flush the border ...
        if border.len() == 3 {
            border.remove(0);
        }
    } else {
        let mut rng = thread_rng();
        let r = rng.gen_range(0, size);
        let p = list.remove(r);
        match b_minidisk(list, border) {
            None => {
                // Should not happen ?
                // An empty circle means we previously had 1 element in our set
                res = Some(Circle::new((p.0, p.1), 0.));
            }
            Some(d) => {
                if d.contains(p) == Outside {
                    border.push(p);
                    res = b_minidisk(list, border);
                } else {
                    res = Some(d);
                }
            }
        }
    }
    res
}

/// Makes a circle form a set of points
fn compute(points: &[(isize, isize)]) -> Option<Circle> {
    match points.len() {
        1 => {
            let p = points.get(0).unwrap();
            Some(Circle::new((p.0, p.1), 0.))
        }
        2 => {
            let p = points.get(0).unwrap();
            let q = points.get(1).unwrap();
            let x = (p.0 + q.0) / 2;
            let y = (p.1 + q.1) / 2;
            let r = (distance_squared(*p, *q) as f64).sqrt() / 2.;
            Some(Circle::new((x, y), r))
        }
        3 => {
            let mut p = points.get(0).unwrap();
            let mut q = points.get(1).unwrap();
            let mut r = points.get(2).unwrap();
            if (*p).1 == (*q).1 {
                p = points.get(2).unwrap();
                r = points.get(0).unwrap();
            } else if (*p).1 == (*r).1 {
                p = points.get(1).unwrap();
                q = points.get(0).unwrap();
            }
            let m_x = 0.5 * ((*p).0 + (*q).0) as f64;
            let m_y = 0.5 * ((*p).1 + (*q).1) as f64;
            let n_x = 0.5 * ((*p).0 + (*r).0) as f64;
            let n_y = 0.5 * ((*p).1 + (*r).1) as f64;
            let alpha1 = ((*q).0 - (*p).0) as f64 / ((*p).1 - (*q).1) as f64;
            let beta1 = m_y - alpha1 * m_x;
            let alpha2 = ((*r).0 - (*p).0) as f64 / ((*p).1 - (*r).1) as f64;
            let beta2 = n_y - alpha2 * n_x;
            let c_x = (beta2 - beta1) / (alpha1 - alpha2);
            let c_y = alpha1 * c_x + beta1;
            let c_radius_squared = ((*p).0 as f64 - c_x) * ((*p).0 as f64 - c_x)
                + ((*p).1 as f64 - c_y) * ((*p).1 as f64 - c_y);
            Some(Circle::new(
                (c_x as isize, c_y as isize),
                c_radius_squared.sqrt(),
            ))
        }
        _ => None,
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_minidisk() {
        let pts0 = vec![];
        let pts1 = vec![(1, 1)];
        let pts2 = vec![(2, 2), (-2, 2)];
        let pts3 = vec![(-1, 0), (0, -1), (1, 0)];

        let c1 = Circle::new((1, 1), 0.);
        let c2 = Circle::new((0, 2), 2.);
        let c3 = Circle::new((0, 0), 1.);

        assert_eq!(compute(&pts0), None);
        assert_eq!(compute(&pts1), Some(c1));
        assert_eq!(compute(&pts2), Some(c2));
        assert_eq!(compute(&pts3), Some(c3));
    }
}
