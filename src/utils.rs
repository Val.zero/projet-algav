use crate::utils::CircleSituation::{InDisk, OnCircle, Outside};
use std::cmp::Ordering;
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader};
use std::path::Path;

/// Returns the square of the distance between two points.
pub fn distance_squared(pt1: (isize, isize), pt2: (isize, isize)) -> isize {
    let x = pt1.0.checked_sub(pt2.0).unwrap_or(isize::MIN);
    let y = pt1.1.checked_sub(pt2.1).unwrap_or(isize::MIN);
    let x_squared = x.checked_mul(x).unwrap_or(isize::MAX);
    let y_squared = y.checked_mul(y).unwrap_or(isize::MAX);
    x_squared.checked_add(y_squared).unwrap_or(isize::MAX)
}

/// Describes a point's situation regarding a circle.
#[derive(Debug, PartialEq)]
pub enum CircleSituation {
    InDisk,
    OnCircle,
    Outside,
}

/// A simple circle.
#[derive(Debug, PartialEq)]
pub struct Circle {
    x: isize,
    y: isize,
    r: f64,
}

impl Circle {
    /// Makes a new circle from a center and a radius.
    pub fn new(center: (isize, isize), radius: f64) -> Circle {
        Circle {
            x: center.0,
            y: center.1,
            r: radius,
        }
    }

    /// Returns the circle's center's x coordinate.
    pub fn get_x(&self) -> isize {
        self.x
    }

    /// Returns the circle's center's y coordinate.
    pub fn get_y(&self) -> isize {
        self.y
    }

    /// Returns the circle's radius.
    pub fn get_radius(&self) -> f64 {
        self.r
    }

    /// Checks if given point is in the disk.
    pub fn contains(&self, point: (isize, isize)) -> CircleSituation {
        let r = self.r * self.r;
        match (distance_squared((self.x, self.y), point) as f64).partial_cmp(&r) {
            None => Outside,
            Some(v) => match v {
                Ordering::Less => InDisk,
                Ordering::Equal => OnCircle,
                Ordering::Greater => Outside,
            },
        }
    }

    /// Checks if every point in list is in the disk.
    pub fn contains_all(&self, points: &[(isize, isize)]) -> bool {
        let mut res = true;
        for p in points {
            match self.contains(*p) {
                InDisk => {}
                OnCircle => {}
                Outside => {
                    res = false;
                    break;
                }
            }
        }
        res
    }
}

/// Parses a sample file into a list of points.
/// Samples must be located in the right dir, named test-n.points (where n is a number), and contain two integers per line.
pub fn parse_sample(n: usize) -> Vec<(isize, isize)> {
    let path_string = format!("./samples/test-{}.points", n);
    let input_path = Path::new(&path_string);
    println!("{:?}", input_path);
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let lines = reader.lines().collect::<io::Result<Vec<String>>>().unwrap();
    let mut res = Vec::new();
    for line in lines {
        let coords = line
            .split(' ')
            .map(|v| v.parse::<isize>().unwrap())
            .collect::<Vec<isize>>();
        let mut iter = coords.iter();
        res.push((*iter.next().unwrap(), *iter.next().unwrap()))
    }
    res
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_distance_sqaured() {
        assert_eq!(distance_squared((0, 0), (0, 0)), 0);
        assert_eq!(distance_squared((0, 0), (1, 0)), 1);
        assert_eq!(distance_squared((0, 0), (1, -1)), 2);
        assert_eq!(distance_squared((0, 0), (0, -1)), 1);
    }

    #[test]
    fn test_contains() {
        let c = Circle::new((0, 0), 4.);
        assert_eq!(c.contains((0, 0)), InDisk);
        assert_eq!(c.contains((4, 0)), OnCircle);
        assert_eq!(c.contains((2, 2)), InDisk);
        assert_eq!(c.contains((4, 4)), Outside);
    }
}
